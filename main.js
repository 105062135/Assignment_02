var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');

var player;
var player2;
var keyboard;

var platforms = [];

var leftWall;
var rightWall;
var ceiling;

var text1;
var text2;
var text3;

var distance = 0;
var status = 'running';

var fuck = new Audio("幹你娘機掰.mp3");
var see = new Audio("看甚麼看.mp3");
var tsao = new Audio("操你媽.mp3")
var sauce = new Audio("sauce.mp3");
var don = new Audio("don.mp3");
var dwai = new Audio("dwai.mp3");
var bgmusic;
var dian = new Audio("dian.mp3");
var end;
var mainstate = {
    preload: function () {
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/player.png', 32, 32);
        game.load.image('wall', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/wall.png');
        game.load.image('ceiling', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/ceiling.png');
        game.load.image('normal', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/normal.png');
        game.load.image('nails', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/nails.png');
        game.load.spritesheet('conveyorRight', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/fake.png', 96, 36);
        game.load.audio('end', 'end.mp3');
    },
    create: function () {
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D 
        });
        bgmusic = game.add.audio('bgmusic');
        bgmusic.play();
        end = game.add.audio('end');
        game.input.keyboard.addKey(Phaser.Keyboard.ESC).onDown.add(this.back, this);
        createBounders();
        createPlayer();
        createTextsBoard();
    },
    update: function () {
        // bad
        if (status == 'gameOver' && keyboard.enter.isDown) restart();
        if (status != 'running') return;

        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        checkTouchCeiling(player);
        checkGameOver();

        updatePlayer();
        updatePlatforms();
        updateTextsBoard();

        createPlatforms();
    },
    back : function(){
        bgmusic.stop();
        game.state.start('menu');
        gameOver();
        game.sound.stopAll();
        status = "running";
    }

};

var mainstate2 = {
    preload: function () {
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/player.png', 32, 32);
        game.load.image('wall', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/wall.png');
        game.load.image('ceiling', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/ceiling.png');
        game.load.image('normal', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/normal.png');
        game.load.image('nails', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/nails.png');
        game.load.spritesheet('conveyorRight', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/fake.png', 96, 36);
        game.load.audio('end', 'end.mp3');
    },
    create: function () {
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D 
        });
        bgmusic = game.add.audio('bgmusic');
        bgmusic.play();
        end = game.add.audio('end');
        game.input.keyboard.addKey(Phaser.Keyboard.ESC).onDown.add(this.back, this);
        createBounders();
        createPlayer2();
        createTextsBoard();
    },
    update: function () {
        // bad
        if (status == 'gameOver' && keyboard.enter.isDown) restart2();
        if (status != 'running') return;

        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        this.physics.arcade.collide(player2, platforms, effect);
        this.physics.arcade.collide(player, player2);
        checkTouchCeiling(player);
        checkTouchCeiling(player2);
        checkGameOver2();

        updatePlayer();
        updatePlayer2();
        updatePlatforms();
        updateTextsBoard();

        createPlatforms();
    },
    back : function(){
        bgmusic.stop();
        game.state.start('menu');
        gameOver();
        game.sound.stopAll();
        status = "running";
    }

};

function createBounders() {
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;
    rightWall = game.add.sprite(383, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.image(0, 0, 'ceiling');
}

var lastTime = 0;
function createPlatforms() {
    if (game.time.now > lastTime + 600) {
        lastTime = game.time.now;
        createOnePlatform();
        distance += 1;
    }
}

function createOnePlatform() {

    var platform;
    var x = Math.random() * (400 - 96 - 40) + 20;
    var y = 400;
    var rand = Math.random() * 100;

    if (rand < 20) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createPlayer() {
    player = game.add.sprite(200, 50, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createPlayer2() {
    player = game.add.sprite(200, 50, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;

    player2 = game.add.sprite(300, 50, 'player');
    player2.direction = 10;
    game.physics.arcade.enable(player2);
    player2.body.gravity.y = 500;
    player2.animations.add('left', [0, 1, 2, 3], 8);
    player2.animations.add('right', [9, 10, 11, 12], 8);
    player2.animations.add('flyleft', [18, 19, 20, 21], 12);
    player2.animations.add('flyright', [27, 28, 29, 30], 12);
    player2.animations.add('fly', [36, 37, 38, 39], 12);
    player2.life = 10;
    player2.unbeatableTime = 0;
    player2.touchOn = undefined;
}

function createTextsBoard() {
    var style = { fill: '#ff0000', fontSize: '20px' }
    text1 = game.add.text(10, 10, '', style);
    text2 = game.add.text(350, 10, '', style);
    text3 = game.add.text(140, 200, 'Enter 重新開始', style);
    text3.visible = false;
}

function updatePlayer() {
    if (keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if (keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    setPlayerAnimate(player);
}

function updatePlayer2() {
    if (keyboard.a.isDown) {
        player2.body.velocity.x = -250;
    } else if (keyboard.d.isDown) {
        player2.body.velocity.x = 250;
    } else {
        player2.body.velocity.x = 0;
    }
    setPlayerAnimate(player2);
}

function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
        player.frame = 8;
    }
}

function updatePlatforms() {
    for (var i = 0; i < platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if (platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard() {
    text1.setText('life:' + player.life);
    text2.setText('B' + distance);
}

function effect(player, platform) {
    if (platform.key == 'conveyorRight') {
        conveyorRightEffect(player, platform);
    }
    if (platform.key == 'conveyorLeft') {
        conveyorLeftEffect(player, platform);
    }
    if (platform.key == 'trampoline') {
        trampolineEffect(player, platform);
    }
    if (platform.key == 'nails') {
        nailsEffect(player, platform);
    }
    if (platform.key == 'normal') {
        basicEffect(player, platform);
    }
    if (platform.key == 'fake') {
        fakeEffect(player, platform);
    }
}

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
    dian.play();
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
    dian.play();
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    dwai.play();
    player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 3;
        fuck.play();
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if (player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
        don.play();
    }
}

function fakeEffect(player, platform) {
    if (player.touchOn !== platform) {
        platform.animations.play('turn');
        tsao.play();
        setTimeout(function () {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}

function checkTouchCeiling(player) {
    if (player.body.y < 0) {
        if (player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if (game.time.now > player.unbeatableTime) {
            player.life -= 3;
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}

function checkGameOver() {
    if (player.life <= 0 || player.body.y > 500) {
        gameOver();
    }
}

function checkGameOver2() {
    if (player.life <= 0 || player2.life <= 0 || player.body.y > 500 ||player2.body.y > 500) {
        gameOver();
    }
}

function gameOver() {
    text3.visible = true;
    bgmusic.stop();
    end.play();
    platforms.forEach(function (s) { s.destroy() });
    platforms = [];
    status = 'gameOver';
}

function restart() {
    text3.visible = false;
    distance = 0;
    end.stop();
    bgmusic.play();
    createPlayer();
    status = 'running';
}

function restart2() {
    text3.visible = false;
    distance = 0;
    end.stop();
    bgmusic.play();
    createPlayer2();
    status = 'running';
}

game.state.add('menu',Menu_State);
game.state.add('game',mainstate);
game.state.add('game2',mainstate2);
game.state.start('menu');