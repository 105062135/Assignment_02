var menu_style = { fill: '#0000ff', fontSize: '30px' };
var unselect_style = { fill: '#ffffff', fontSize: '30px' };
var select_one_in_menu = 0;
var menumusic;
var selectmusic;
var Coverphoto;
var menu_StartGame;
var menu_2Players;
var menu_leaderboard;
var Menu_State = {
    preload: function () {
        game.load.image('coverphoto', '台妹.jpg');
        game.load.audio('menu_music', 'menu.mp3');
        game.load.audio('bgmusic', 'bgmusic.mp3');
    },

    create: function () {
        menumusic = game.add.audio('menu_music');
        selectmusic = game.add.audio('select_music');
        Coverphoto = game.add.image(0, 0, 'coverphoto');
        Coverphoto.scale.setTo(0.5, 0.5);
        menumusic.play();
        menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
        menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
        menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        if (select_one_in_menu == 0) {
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        } else if (select_one_in_menu == 1) {
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', menu_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        } else if (select_one_in_menu == 2) {
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', menu_style);
        }
        game.input.keyboard.addKey(Phaser.Keyboard.UP).onDown.add(this.previosoption, this);
        game.input.keyboard.addKey(Phaser.Keyboard.DOWN).onDown.add(this.nextoption, this);
        game.input.keyboard.addKey(Phaser.Keyboard.W).onDown.add(this.previosoption, this);
        game.input.keyboard.addKey(Phaser.Keyboard.S).onDown.add(this.nextoption, this);
        var EnterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        EnterKey.onDown.add(this.startgame, this);
    },
    nextoption: function () {
        selectmusic.play();
        if (select_one_in_menu == 0) {
            select_one_in_menu = 1;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', menu_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        } else if (select_one_in_menu == 1) {
            select_one_in_menu = 2;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', menu_style);
        } else if (select_one_in_menu == 2) {
            select_one_in_menu = 0;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        }
    },
    previosoption: function () {
        selectmusic.play();
        if (select_one_in_menu == 0) {
            select_one_in_menu = 2;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', menu_style);
        } else if (select_one_in_menu == 1) {
            select_one_in_menu = 0;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        } else if (select_one_in_menu == 2) {
            select_one_in_menu = 1;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', menu_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        }
    },
    startgame: function () {
        menumusic.stop();
        if (select_one_in_menu == 0) {
            menumusic.stop();
            game.state.start('game');
        } else if (select_one_in_menu == 1) {
            game.state.start('game2');
        } else if (select_one_in_menu == 2) {
            game.state.start('leaderboard');
        }

    }
}